#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_set>

using namespace std;

enum InputType {WORD, RESULT};

const string ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
const string ACCURACY = "-*!";

string toString(char charArray[])
{
	string outString;
	for (int i = 0; i < 5; i++)
	{
		outString += charArray[i];
	}
	return outString;
}

void populateWords (vector<string> &wordList)
{
	fstream infile;
	
	//nfile.open("list.txt", ios::in); //sorted alphabetically
	infile.open("count_1w.txt", ios::in); //sorted by frequency
	//infile.open("en_wikt_words_1_5-5.txt", ios::in); //sorted by frequency
	//infile.open("poplist.txt", ios::in); //curated frequency list
	//infile.open("letFreqList.txt", ios::in); //sorted by score based on letter frequency
	if (infile.is_open())
	{
		string line;
		while (getline(infile, line))
		{
			string word = line.substr(0, line.find('\t'));
			if (word.length() == 5 && word.find_first_not_of(ALPHA) == string::npos)
			{
				wordList.push_back(word);
			}
		}
		cout << wordList[0] << endl;
	}
	
	infile.close();
}

void processMiss(char missLetter, vector<string> &words)
{
	vector<string> tempWords;
	
	for (int i = 0; i < words.size(); i++)
	{
		if (words[i].find(missLetter) == string::npos)
		{
			tempWords.push_back(words[i]);
		}
	}
	
	if (tempWords.size() > 0)
	{
		words.clear();
		words.assign(tempWords.begin(), tempWords.end());
	}
	
	return;
}

void processHit(char hitLetter, int location, vector<string> &words)
{
	vector<string> tempWords;
	
	for (int i = 0; i < words.size(); i++)
	{
		if (words[i][location] == hitLetter)
		{
			tempWords.push_back(words[i]);
		}
	}
	
	if (tempWords.size() > 0)
	{
		words.clear();
		words.assign(tempWords.begin(), tempWords.end());
	}
	
	return;
}

void processClose(char closeLetter, int location, vector<string> &words)
{
	vector<string> tempWords;
	
	for (int i = 0; i < words.size(); i++)
	{
		if (words[i][location] != closeLetter && words[i].find(closeLetter) != string::npos)
		{
			tempWords.push_back(words[i]);
		}
	}
	
	if (tempWords.size() > 0)
	{
		words.clear();
		words.assign(tempWords.begin(), tempWords.end());
	}
	
	return;
}

int validateInput(vector<string> words, string input, InputType inputType)
{
	if (input.length() != 5)
	{
		cout << "Error! Input must be exactly 5 characters long. Please enter a valid input." << endl;
		return 0;
	}

	if (inputType == WORD)
	{
		if (input.find_first_not_of(ALPHA) != string::npos)
		{
			cout << "Error! Guess can only contain English letters. Please enter a valid input." << endl;
			return 0;
		}

		if (find(words.begin(), words.end(), input) == words.end())
		{
			cout << "Error! Guess is not in the list of valid English words. Please enter a valid English word." << endl;
			return 0;
		}
	}
	else
	{
		if (input.find_first_not_of(ACCURACY) != string::npos)
		{
			cout << "Error! Accuracy can only contain the characters -, *, and !. Please enter a valid accuracy." << endl;
			return 0;
		}
	}

	return 1;
}

int solveWordle()
{
	vector<string> words;
	string letters = "-----";
	string result = "-----";
	unordered_set<char> hitCloseLetters = {};
	int guesses = 1;

	//populate inital word list
	populateWords(words);

	//start main loop
	while (guesses <= 6)
	{
		if (guesses == 1)
		{
			cout << "Please enter your first guess:" << endl;
		}
		else
		{
			cout << "Please enter your next guess:" << endl;
		}

		int validWord = 0;
		while (validWord != 1)
		{
			cin >> letters;
			validWord = validateInput(words, letters, WORD);

		}

		cout << "Please enter the accuracy of the guess. - for miss, * for yellow, ! for green." << endl;

		int validResult = 0;
		while (validResult != 1)
		{
			cin >> result;
			validResult = validateInput(words, result, RESULT);
		}

		if (result == "!!!!!")
		{
			cout << "You win! The word was: " << result << endl;
			return 1;
		}

		for (int i = 0; i < 5; i++)
		{
			if (result[i] == '!')
			{
				hitCloseLetters.insert(letters[i]);
				processHit(letters[i], i, words);
			}
			else if (result[i] == '*')
			{
				hitCloseLetters.insert(letters[i]);
				processClose(letters[i], i, words);
			}
		}

		for (int i = 0; i < 5; i++)
		{
			if (result[i] == '-')
			{
				if (hitCloseLetters.find(letters[i]) == hitCloseLetters.end())
				{
					processMiss(letters[i], words);
				}
				else
				{
					processClose(letters[i], i, words);
				}
			}
		}

		//cout << words.size() << endl;
		for (int i = 0; i < 10; i++)
		{
			if (words.size() > i)
			{
				cout << words[i] << endl;
			}
		}

		guesses++;
	}

	cout << "You ran out of guesses and lost!" << endl;
	return 0;
}

int playWordle()
{
	vector<string> words;
	string guess = "-----";
	string letters = "a b c d e f g h i j k l m n o p q r s t u v w x y z";
	vector<string> results = {"+-------+\n", "| ----- |\n", "| ----- |\n", "| ----- |\n", "| ----- |\n", "| ----- |\n", "| ----- |\n", "+-------+\n"};
	int guesses = 1;

	//populate inital word list
	populateWords(words);

	srand(time(0));
	string word = words[rand() % words.size()];

	cout << "The magic word is: " << word << endl;

	//start main loop
	while (guesses <= 6)
	{
		if (guesses == 1)
		{
			cout << "Please enter your first guess:" << endl;
		}
		else
		{
			cout << "Please enter your next guess:" << endl;
		}

		int validWord = 0;
		while (validWord != 1)
		{
			cin >> guess;
			validWord = validateInput(words, guess, WORD);

		}

		results[guesses].clear();
		results[guesses] += "| ";
		for (int i = 0; i < 5; i++)
		{
			if (guess[i] == word[i])
			{
				results[guesses] += "\x1B[32m";
				results[guesses] += guess[i];
				results[guesses] += "\033[0m";

				letters.insert(letters.find(guess[i]), "\x1B[32m");
				letters.insert(letters.find(guess[i]) + 1, "\033[0m");
				//result[i] = '!';
			}
			else if (word.find(guess[i]) != string::npos)
			{
				results[guesses] += "\x1B[33m";
				results[guesses] += guess[i];
				results[guesses] += "\033[0m";

				letters.insert(letters.find(guess[i]), "\x1B[33m");
				letters.insert(letters.find(guess[i]) + 1, "\033[0m");
				//result[i] = '*';
			}
			else
			{
				results[guesses] += guess[i];

				letters.insert(letters.find(guess[i]), "\x1B[31m");
				letters.insert(letters.find(guess[i]) + 1, "\033[0m");
				//result[i] = '-';
			}
		}
		results[guesses] += " |\n";

		system("cls");
		cout << letters << endl;
		for (vector<string>::iterator iter = results.begin(); iter != results.end(); iter++)
		{
			printf((*iter).c_str());
		}

		if (guess == word)
		{
			cout << "You win! The word was: " << word << endl;
			return 1;
		}

		guesses++;
	}

	cout << "You ran out of guesses and lost!" << endl;
	cout << "The word was: " << word << endl;
	return 0;
}

int main()
{
	cout << "+-------------------------------------+" << endl;
	cout << "| Welcome to the Wordle Helper!       |" << endl;
	cout << "| This program will offer suggestions |" << endl;
	cout << "| based on your current progress. You |" << endl;
	cout << "| can also use this program to play   |" << endl;
	cout << "| Wordle.                             |" << endl;
	cout << "|                                     |" << endl;
	cout << "| Please select an option:            |" << endl;
	cout << "| 1 - Solve Wordle                    |" << endl;
	cout << "| 2 - Play Wordle                     |" << endl;
	cout << "| 3 - Exit                            |" << endl;
	cout << "+-------------------------------------+" << endl;

	char input;
	do
	{
		cin >> input;
		switch (input)
		{
			case '1':
				solveWordle();

				cout << "Would you like to play again? (Y/N)" << endl;
				cin >> input;

				if (input == 'Y' || input == 'y')
				{
					input = 1;
				}
				else
				{
					input = 3;
				}
				break;
			case '2':
				playWordle();

				cout << "Would you like to play again? (Y/N)" << endl;
				cin >> input;
				
				if (input == 'Y' || input == 'y')
				{
					system("cls");

					cout << "+-------------------------------------+" << endl;
					cout << "| Welcome to the Wordle Helper!       |" << endl;
					cout << "| This program will offer suggestions |" << endl;
					cout << "| based on your current progress. You |" << endl;
					cout << "| can also use this program to play   |" << endl;
					cout << "| Wordle.                             |" << endl;
					cout << "|                                     |" << endl;
					cout << "| Please select an option:            |" << endl;
					cout << "| 1 - Solve Wordle                    |" << endl;
					cout << "| 2 - Play Wordle                     |" << endl;
					cout << "| 3 - Exit                            |" << endl;
					cout << "+-------------------------------------+" << endl;

					input = 2;
				}
				else
				{
					input = 3;
				}
				break;
			case '3':
				break;
			default:
				cout << "!!! Please enter a valid menu selection. !!!" << endl;
				break;
		}
	} while (input != 3);
	
	return 0;
}